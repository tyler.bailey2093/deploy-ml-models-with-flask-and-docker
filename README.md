# Deploy ML Models with Flask and Docker

![](./resources/docker_flask.png)

## **Goal**
This project focuses on a few tasks. The goal is to perform feature engineering, optimize a classification model, prepare code for production, call the model from an API using flask, wrap the code in a Docker container, and optimize the code further for scalability.

## **Objective - Details**
### Step 1: Optimization
Look for opportunities to improve  performance including data cleaning/preparation, model selection, train/test split, and hyper-parameter tuning. The model performance will be measured by AUC against the holdout test set.

### Step 2: Productionize
Prepare model deployment for production: Update your code to meet common production coding standards and best practices. These include modularization, code quality, proper unit testing, and comments/documentation.

### Step 3: API
Wrap the model code inside an API: The model must be made callable via API call. The call will pass 1 to N rows of data in JSON format, and expects a N responses each with a predicted class and probability belonging to the predicted class.

Example curl call to API:

curl --request POST --url http://localhost:5001/predict --header 'content-type: application/json' --data '{"x0": "9.521496806", "x1": "wed", "x2": "-5.087588682", "x3": "-17.21471427", ..., "x97": "2.216918955", "x98": "-18.64465705", "x99": "-1.926577376"}'

Sample JSON Response:
[{"class": 0, "probability": 0.5211449595189876}]

or a batch curl call:

curl --request POST --url http://localhost:5001/predict --header 'content-type: application/json' --data '[{"x0": "9.521496806", "x1": "wed", "x2": "-5.087588682", "x3": "-17.21471427", ..., "x97": "2.216918955", "x98": "-18.64465705", "x99": "-1.926577376"},{"x0": "8.415753628", "x1": "thur", "x2": "-4.934359322", "x3": "-6.21844247", ..., "x97": "6.2714321", "x98": "-38.057369", "x99": "-2.76817620"},...,{"x0": "0.96691828", "x1": "thursday", "x2": "-3.86881782", "x3": "-2.2981827", ..., "x97": "3.1854471", "x98": "-33.6058873", "x99": "-2.02788172"}]'

Sample Batch JSON Response:
[{"class": 0, "probability": 0.5211449595189876},
                {"class": 1, "probability": 0.6211449595189876},
                {"class": 0, "probability": 0.7211449595189876},
		...
                {"class": 0, "probability": 0.8211449595189876}]
                

Each of the 10,000 rows in the test dataset will be passed through an API call. The call could be a single batch call w/ all 10,000 rows, or 10,000 individual calls. API should be able to handle either case with minimal impact to performance. Note that whether it is a single call or batch call, it should always return an array.

### Step 4: Dockerize
Wrap API in a Docker image: Create a Dockerfile that builds the API into an image. Write a shell script that either runs your image using traditional docker run commands or orchestrates your deployment using Compose, Swarm or Kubernetes (include relevant *.yml config files).

### Step 5: Scale
Optimize deployment for enterprise production and scalability: Identify opportunities to optimize deployment for scalability. Consider how the API might handle a large number of calls (thousands per minute). What additional steps/tech could be added to the deployment in order to make it scalable for enterprise level production.

## **Test the application**
Run the following to clone the repo and navigate to the app.
```
$ git clone https://gitlab.com/tyler.bailey2093/deploy-ml-models-with-flask-and-docker.git

$ cd deploy-ml-models-with-flask-and-docker/app
```

Next, build out the Docker container with relevant dependencies.
```
$ sudo bash run_api.sh
```

Check that the image(s) were built. 
```
$ docker images
```

Make sure the server status is "up" and the TCP port is 5001.
```
$ docker ps -a
```

Run the python command to process a POST request to the server.
```
$ python server.py
```

### Clean up
Finally, run the following to stop and remove the container.
```
$ docker stop <CONTAINER_NAME>
$ docker rmi -f <IMAGE_ID>
```

And that's it!

