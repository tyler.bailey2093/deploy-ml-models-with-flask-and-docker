import sys
import os
import traceback
import json
import pandas as pd
from flask import Flask, request, jsonify
from flask_cors import CORS
from joblib import load

try:
    print('Loading model...')
    rfc = load("model/rfc_model.pkl")
    print('Model loaded!')
    print('/nLoading columns..')
    model_columns = load("model/model_cols.pkl")
    cat_cols = load("model/category_cols.pkl")
    print('Model columns loaded!')
except:
    print('Error loading application. Please run main.py')
    sys.exit(0)

app = Flask(__name__)
CORS(app)

@app.route('/predict', methods=['POST', 'GET'])
def predict():
    if rfc:
        try:
            json_ = request.get_json()
            response = json.loads(json_)

            for i in response:
                rep1 = pd.DataFrame(response)
                #TODO: Call transform on categorical columns, performing OHE for now.
                rep2 = pd.get_dummies(rep1.fillna(rep1.mean()))
                rep3 = rep2.reindex(columns=model_columns, fill_value=0)                    
                
                predicted_class = rfc.predict(rep3)
                probabilities = rfc.predict_proba(rep3)
                
                res = {'class': predicted_class.tolist(), 'probabilities': probabilities.tolist()}
                content = [dict(zip(res.keys(), i)) for i in zip(*res.values())]

            return jsonify(content)

        except:
            return jsonify({'trace': traceback.format_exc()})
    else:
        print('Train the model first.')
        return ('No model here to use.')


if __name__ == '__main__':

    try:
        port = int(os.getenv("PORT", 5001))
        app.run(host='0.0.0.0', port=port, debug=True)
    except Exception as e:
        print('Whoops, looks like PORT 5001 may not be available, try again in a few minutes.', e)
