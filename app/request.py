import sys
import pandas as pd
import requests
import json
import os

def main():
    """Test payload.
    """
    dirname = os.path.dirname(__file__)
    file_name = os.path.join(dirname, '../Data/test.csv')

    df = pd.read_csv(file_name, encoding="utf-8-sig")
    df = df.head(5)
    data = df.to_json(orient='records')

    header = {'Content-Type': 'application/json', \
                    'Accept': 'application/json'}

    resp = requests.post("http://0.0.0.0:5001/predict", \
                        data = json.dumps(data),\
                        headers = header)

    print('\nLabel Probabilities')
    print('-----------------------')
    print(resp.json())


if __name__ == '__main__':
    main()