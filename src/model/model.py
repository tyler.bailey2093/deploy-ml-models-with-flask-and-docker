from joblib import dump
from sklearn.ensemble import RandomForestClassifier


class CustomModel(object):

    def __init__(self, n_estimators, max_depth, params=None):
        self.n_estimators = n_estimators
        self.max_depth = max_depth
        self.params = params

    def fit(self, X_train, y_train):
        if self.params:
            rfc = RandomForestClassifier(**self.params)
        else:
            rfc = RandomForestClassifier(bootstrap=True,
                                         criterion='gini',
                                         max_depth=self.max_depth,
                                         n_estimators=self.n_estimators,
                                         n_jobs=-1)
        print('\nModel is developing...')
        rfc.fit(X_train, y_train)
        print('\nModel finished training.')

        return rfc

    def save_model(self, model, file_name, train_x=None):
        if train_x:
            model_columns = list(train_x.columns)
            dump(model_columns, 'model_columns.pkl')

        dump(model, file_name)
