from __future__ import division

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from imblearn.over_sampling import SMOTE
from matplotlib.legend_handler import HandlerLine2D
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import LinearSVC
from sklearn.utils import resample


def base_recursive_feature_selection(train_x, train_y, num_features=None, params=None):
    if params:
        model = LogisticRegression(**params)
    else:
        model = LogisticRegression(solver='lbfgs')
    if num_features:
        rfe = RFE(model, n_features_to_select=num_features, step=1)
    else:
        rfe = RFE(model, n_features_to_select=30, step=1)

    rfe = rfe.fit(train_x, train_y)
    return rfe


def svc_recursive_feature_selection(X_train, y_train, num_features=None, params=None):
    if params:
        svm = LinearSVC(**params)
    else:
        svm = LinearSVC()
    if num_features:
        rfe = RFE(svm, n_features_to_select=num_features)
    else:
        n = int(len(X_train.columns))
        rfe = RFE(svm, n_features_to_select=n)

    svc_rfe = rfe.fit(X_train, y_train)
    return svc_rfe


def statsmodels_pvalue_feature_selection(train_x, train_y):
    X_1 = sm.add_constant(train_x)
    model = sm.OLS(train_y, X_1).fit()
    model_pvalues = model.pvalues
    model_pvalues_df = pd.DataFrame({'feature': model_pvalues.index, 'p_values': model_pvalues.values})
    filtered_model_pv = model_pvalues_df[model_pvalues_df['p_values'] <= 0.05]
    important_features = filtered_model_pv['feature'].tolist()

    return important_features


def scaler_train_test_split(df_raw, x_test, raw_y, len_train, feature_range=[0, 1], test_size=0.3):
    scaler = MinMaxScaler(feature_range=feature_range)
    raw_rescaled = scaler.fit_transform(df_raw)
    scaled_df = pd.DataFrame(raw_rescaled, columns=x_test.columns)
    x = scaled_df[:len_train]
    y = raw_y[:len_train]
    train_x, test_x, train_y, test_y = train_test_split(x, y, test_size=test_size, random_state=42)
    return train_x, test_x, train_y, test_y


def rf_random_grid_search(train_x, train_y, n_iter=100, cv=5, start=50, stop=2000, num=10, max_depth_n=11):
    # Number of trees in random forest
    n_estimators = [int(x) for x in np.linspace(start=start, stop=stop, num=num)]
    # Number of features to consider at every split
    max_features = ['auto', 'sqrt']
    # Maximum number of levels in tree
    max_depth = [int(x) for x in np.linspace(10, 110, num=max_depth_n)]
    max_depth.append(None)
    # Minimum number of samples required to split a node
    min_samples_split = [2, 5, 10]
    # Minimum number of samples required at each leaf node
    min_samples_leaf = [1, 2, 4]
    # Method of selecting samples for training each tree
    bootstrap = [True, False]
    # Create the random grid
    random_grid = {'n_estimators': n_estimators,
                   'max_features': max_features,
                   'max_depth': max_depth,
                   'min_samples_split': min_samples_split,
                   'min_samples_leaf': min_samples_leaf,
                   'bootstrap': bootstrap}

    # Using the random grid to search for best hyperparameters
    rf = RandomForestRegressor()
    rf_random = RandomizedSearchCV(estimator=rf,
                                   param_distributions=random_grid,
                                   n_iter=n_iter,
                                   cv=cv,
                                   random_state=42,
                                   n_jobs=-1)
    # Fit the random search model
    rf_random.fit(train_x, train_y)
    # print("Random Forest-Random Grid Search Best Params: {}".format(rf_random.best_params_))
    return rf_random


def cv_grid_search(estimator, train_x, train_y, param_grid, cv=5, params=None):
    if params:
        CV = GridSearchCV(**params)
    else:
        CV = GridSearchCV(estimator=estimator, param_grid=param_grid, cv=cv)

    CV.fit(train_x, train_y)
    best_estimator = CV.best_params_.values()[0]

    return CV, best_estimator


def get_best_estimator(train_x, train_y, test_x, test_y):
    n_estimators = [1, 2, 4, 8, 16, 32, 64, 100, 200]
    train_results = []
    test_results = []
    for estimator in n_estimators:
        rf = RandomForestClassifier(n_estimators=estimator, n_jobs=-1)
        rf.fit(train_x, train_y)
        train_pred = rf.predict(train_x)
        false_positive_rate, true_positive_rate, thresholds = roc_curve(train_y, train_pred)
        roc_auc = auc(false_positive_rate, true_positive_rate)
        train_results.append(roc_auc)
        y_pred = rf.predict(test_x)
        false_positive_rate, true_positive_rate, thresholds = roc_curve(test_y, y_pred)
        roc_auc = auc(false_positive_rate, true_positive_rate)
        test_results.append(roc_auc)

    line1, = plt.plot(n_estimators, train_results, 'b', label="Train AUC")
    line2, = plt.plot(n_estimators, test_results, 'r', label="Test AUC")
    plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})
    plt.ylabel('AUC score')
    plt.xlabel('n_estimators')
    plt.show()


def get_resample(train_x, train_y):
    # concatenate training data back
    X = pd.concat([train_x, train_y], axis=1)

    # separate minority and majority classes
    neg = X[X.y == 0]
    pos = X[X.y == 1]

    # upsample minority
    upsampled_m = resample(pos,
                           replace=True,
                           n_samples=len(neg),
                           random_state=27)

    # combine majority and upsampled minority
    upsampled = pd.concat([neg, upsampled_m])

    return upsampled


def get_smote_samples(train_x, train_y):
    # Synthetic smote sampling
    sm = SMOTE(random_state=27, ratio=1.0)
    train_x, train_y = sm.fit_sample(train_x, train_y)
    return train_x, train_y
