import copy

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from utils.utility_functions import remove_dollar, per_float, toYears


class Transform(object):
    """
    This class accepts train and test data as csv's and transforms each for model deployment.
    """

    def __init__(self, custom_features=None):
        self.custom_features = custom_features

    def load_data(self, raw_train, raw_test):
        """
        Load csv data.
        
        :return: Returns two pandas dataframes
        :rtype: pandas.core.frame.DataFrame
        """
        raw_train = pd.read_csv(raw_train)
        raw_test = pd.read_csv(raw_test)

        return raw_train, raw_test

    def feature_selection(self, raw_train, raw_test):
        """
        :param raw_train: Training dataset
        :type raw_train: pandas.core.frame.DataFrame
        :param raw_test: Test dataset
        :type raw_test: pandas.core.frame.DataFrame
        
        :return: Returns transformed features as well as separate pred col
        :rtype: pandas.core.frame.DataFrame, pandas.core.series.Series
        """
        # Drop NULL values in column x1
        raw_train_drop = raw_train.dropna(axis=0, subset=['y'])

        len_train = len(raw_train_drop)
        len_test = len(raw_test)

        # Combine train and test data
        list_all = [raw_train_drop, raw_test]
        raw = pd.concat(list_all, ignore_index=True, sort=True)

        del (raw_train, raw_test, raw_train_drop)

        # Get percentage of NULL values for each feature
        null_list = raw.isnull().sum().sort_values(ascending=False) / float(raw.shape[0]) * 100

        # remove features not used for modeling
        del raw['x2']
        del raw['x3']
        del raw['x19']

        # These feature need Nature Language Processing before using, thus increasing the complexity of current model
        del raw['x10']
        del raw['x16']
        del raw['x18']

        # Remove redundant feature
        del raw['x8']

        # Remove dollar sign
        raw['x4'] = raw['x4'].apply(remove_dollar)
        raw['x5'] = raw['x5'].apply(remove_dollar)
        raw['x6'] = raw['x6'].apply(remove_dollar)
        raw['x12'] = raw['x12'].apply(remove_dollar)

        raw['x30'] = raw['x30'].apply(per_float)

        # Create new features to be used in modeling
        raw['x33'] = raw['x5'] / raw['x4']
        raw['x34'] = raw['x6'] / raw['x5']

        raw['x15'] = raw['x15'].apply(toYears)
        raw['x23'] = raw['x23'].apply(toYears)

        # Time difference between issue date and the date opened
        raw['x35'] = raw['x15'] - raw['x23']

        ### Set target variable and remove it from input variable list
        raw_y = raw['y']
        del raw['y']

        return raw, raw_y, len_train, len_test

    def categorical_transformation(self, raw):
        """
        :param raw: Transformed dataset from feature selection method
        :type: pandas.core.frame.DataFrame
        
        :return: Encoded categorical features dataset + numerical + categorical cols
        :rtype: pandas.core.frame.DataFrame, list, list
        """
        cat_cols = raw.dtypes[raw.dtypes == 'object'].index
        num_cols = raw.dtypes[raw.dtypes == 'float64'].index

        # Make a copy of raw input, will be used later as input variables in the linear regression model 
        raw_bp_linear = copy.deepcopy(raw)

        # Replace the Null value with very large number (10**20), let tree model to interpret by itself
        for i in num_cols:
            raw[i].fillna(10 ** 20, inplace=True)

        # Label encoding for categorical feature
        LBL = preprocessing.LabelEncoder()
        dict_list = []
        for i in cat_cols:
            raw[i] = LBL.fit_transform(raw[i].fillna('0'))
            j = dict(zip(np.arange(len(LBL.classes_)), LBL.classes_))
            k = {i: j}
            dict_list.append(k)

        return raw

    def split(self, raw, raw_y, len_train, test_size=0.3, random_state=42):
        """
        Split into training and test dataset
        
        :param raw: Raw dataset
        :type raw: pandas.core.frame.DataFrame
        :param raw_y: Y pred pandas series
        :type raw_y: pandas.core.series.Series
        :param len_train: length of train dataset
        :type len_train: int
        :param test_size: determines split of test and train sets
        :type test_size: float
        
        :return: train and test datasets
        :rtype: pandas.core.frame.DataFrame
        """

        if self.custom_features:
            if isinstance(self.custom_features, list):
                raw = raw[[c for c in raw.columns if c in self.custom_features]]
            else:
                raise ValueError("Custom features must be a list")

        x = raw[:len_train]
        y = raw_y[:len_train]
        train_x, test_x, train_y, test_y = train_test_split(x, y, test_size=test_size, random_state=random_state)
        holdout_x = raw[len_train:]
        holdout_y = raw_y[len_train:]

        return train_x, test_x, train_y, test_y
